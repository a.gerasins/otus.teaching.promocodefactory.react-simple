import './App.css';
import Authentication from './pages/Authentication';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Layout from './hoc/Layout';
import Home from './pages/Home';
import Register from './pages/Register';
import NoFound from './pages/404';

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/login">
            <Authentication />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/">
            <NoFound />
          </Route>
        </Switch>

      </Layout>
    </BrowserRouter>
  );
}

export default App;
