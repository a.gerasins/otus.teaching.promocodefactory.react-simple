import React from 'react'
import {useDispatch} from 'react-redux';
import {useShallowEqualSelector} from '../../utils/redux';

const Home = () => {
  const counter = useShallowEqualSelector(state => state.counter)
  const dispatch = useDispatch();
  return (
      <div className="container">
        <h1>Home</h1>
        <div>Счетчик <strong>{counter}</strong></div>
        <div className="mt-2">
          <button className="btn btn-primary me-2" onClick={() => dispatch({type: 'ADD'})}>Добавить 1</button>
          <button className="btn btn-primary" onClick={() => dispatch({type: 'SUB'})}>Вычесть 1</button>
        </div>

      </div>
  );
};

export default Home;