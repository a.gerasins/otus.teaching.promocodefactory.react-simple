import React from 'react'
import {Link} from 'react-router-dom';

const Layout = (props) => {
  return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <Link to="/" className="navbar-brand"  aria-current="page">Navbar</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link to="/" className="nav-link active"  aria-current="page">Home</Link>
                </li>
                <li className="nav-item">
                  <Link to="/abracadabra" className="nav-link active"  aria-current="page">404</Link>
                </li>
              </ul>
              <ul className="d-flex navbar-nav mb-lg-0">
                <li className="nav-item">
                  <Link to="/login" className="nav-link active"  aria-current="page">Login</Link>
                </li>
                <li className="nav-item">
                  <Link to="/register" className="btn btn-primary"  aria-current="page">Register</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        {props.children}
      </div>
  );
};

export default Layout;