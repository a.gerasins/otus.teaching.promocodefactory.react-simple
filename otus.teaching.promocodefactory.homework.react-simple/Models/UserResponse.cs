﻿using System;

namespace otus.teaching.promocodefactory.homework.react_simple.Models
{
    public class UserResponse
    {
        public Guid Token { get; set; }
    }
}