﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace otus.teaching.promocodefactory.homework.react_simple.Models
{
    public class UserInput
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
