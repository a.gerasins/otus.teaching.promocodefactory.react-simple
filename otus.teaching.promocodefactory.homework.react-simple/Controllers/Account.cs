﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using otus.teaching.promocodefactory.homework.react_simple.Models;

namespace otus.teaching.promocodefactory.homework.react_simple.Controllers
{
    [Route("api/[controller]/login")]
    [ApiController]
    public class Account : ControllerBase
    {
        [HttpPost]
        public ActionResult<UserResponse> Login(UserInput user)
        {
            if (user == null)
                return NotFound();

            var result = new UserResponse()
            {
                Token = System.Guid.NewGuid()
            };

            return Ok(result);
        }
    }
}
    
